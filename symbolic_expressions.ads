----------------------------------------------------------------------------
--            Symbolic Expressions (symexpr)
--
--               Copyright (C) 2012, Riccardo Bernardini
--
--      This file is part of symexpr.
--
--      symexpr is free software: you can redistribute it and/or modify
--      it under the terms of the Lesser GNU General Public License as published by
--      the Free Software Foundation, either version 3 of the License, or
--      (at your option) any later version.
--
--      symexpr is distributed in the hope that it will be useful,
--      but WITHOUT ANY WARRANTY; without even the implied warranty of
--      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--      GNU General Public License for more details.
--
--      You should have received a copy of the Lesser GNU General Public License
--      along with gclp.  If not, see <http://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

--
-- <summary>
--  I wrote the first version of this package a day when I wanted to allow
--  the user of a program to specify values in a symbolic form like
--  "max(intro.end, bluesheet.end)"  After writing the code that handled this
--  type of expression, I noticed that the code was general enough and with
--  minimal effort I converted it to a generic package that I succesively
--  improved.
--
--  This package allows you to handle expression in "symbolic" form inside
--  your program.  Expressions operate on "scalars" whose type parametrizes
--  this package (so you can have expressions of floats, integers or...
--  dates [my case]).
--
--  You can
--
--   (A)  Construct symbolic expression
--         =>  by parsing strings with Parse, e.g.,
--
--                 X := parse("2*3 - max(y, abs(u))")
--
--         =>  by constructing them using the provided constructors and
--             operators.  For example,
--
--                 X    := Variable("x");
--                 Poly := to_expr(12)*X*X - to_expr(4)*x + to_expr(1);
--                 Top  := Function_Call("max", Function_Call("abs", x), Poly);
--
--              in this case Top is an expression that represents the function
--
--                   max(abs(x), 12*x*x - 4*x + 1)
--
--    (B)  Replace variables in expression with scalars or other
--         expressions (function Replace)
--
--    (C)  Evaluate an expression.  For example, to plot a graph of Top(X)
--         you can use
--
--            for I in 1..10 loop
--              Plot(I, Eval(Replace(Top, "x", I)));
--            end loop;
--
--  As said above, this package is parameterized by the type of the scalar.
--  It requires that the usual operators (+, -, *, ...) are defined.
-- </summary>

with Ada.Strings.Unbounded;
with Ada.Finalization;
with Ada.Containers.Indefinite_Ordered_Maps;
with Ada.Containers.Indefinite_Ordered_Sets;

--
--
--

generic
   type Scalar_Type is  private;
   --  The type representing the scalars

   type Scalar_Array is array (Positive range <>) of Scalar_Type;

   with function "+" (X : Scalar_Type) return Scalar_Type is <>;
   with function "-" (X : Scalar_Type) return Scalar_Type is <>;

   with function "+" (L, R : Scalar_Type) return Scalar_Type is <>;
   with function "-" (L, R : Scalar_Type) return Scalar_Type is <>;
   with function "*" (L, R : Scalar_Type) return Scalar_Type is <>;
   with function "/" (L, R : Scalar_Type) return Scalar_Type is <>;

   with function Call (Name : String; Param : Scalar_Array)
                       return Scalar_Type is <>;
   --  Callback used to evaluate the functions in a Symoblic_Expression
   --  Name is the name of the function, while Param will store the
   --  parameters given to the function

   with procedure Read_Scalar (Input    : in     String;
                               Success  :    out Boolean;
                               Consumed :    out Natural;
                               Result   :    out Scalar_Type);
   --  If Input begins with a valid scalar value, set Success to True,
   --  Result to the corresponding scalar value and Consumed to the
   --  number of chars that make the text representation of the scalar.
   --  This procedure is used by parse.

   with function Image (Item : Scalar_Type) return String;
   --  Return a text representation of a scalar
package Symbolic_Expressions is
   type Symbolic_Expression is new
     Ada.Finalization.Controlled
   with
     private;

   type Expression_Array is array (Positive range <>) of Symbolic_Expression;


   function Is_Constant (X : Symbolic_Expression) return Boolean;
   --  Return true if X has no free variables

   Not_A_Scalar : exception;
   function Eval (X : Symbolic_Expression) return Scalar_Type;
   --  Evaluate expression X and return the corresponding scalar value.
   --  Raise Not_A_Scalar is Is_Constant(X) is false


   function To_Expr (X : Scalar_Type) return Symbolic_Expression;
   --  Return an expression representing the value of X

   function Variable (Name : String) return Symbolic_Expression;
   -- Return an expression representing the given variable

   function Function_Call (Function_Name : String;
                           Parameters    : Expression_Array)
                           return Symbolic_Expression;
   -- Return an expression representing a function call

   function "+" (L : Symbolic_Expression) return Symbolic_Expression;
   function "-" (L : Symbolic_Expression) return Symbolic_Expression;

   function "+" (L, R : Symbolic_Expression) return Symbolic_Expression;
   function "-" (L, R : Symbolic_Expression) return Symbolic_Expression;
   function "*" (L, R : Symbolic_Expression) return Symbolic_Expression;
   function "/" (L, R : Symbolic_Expression) return Symbolic_Expression;

   function "+" (L : Symbolic_Expression; R : Scalar_Type) return Symbolic_Expression;
   function "-" (L : Symbolic_Expression; R : Scalar_Type) return Symbolic_Expression;
   function "*" (L : Symbolic_Expression; R : Scalar_Type) return Symbolic_Expression;
   function "/" (L : Symbolic_Expression; R : Scalar_Type) return Symbolic_Expression;
   function "+" (L : Scalar_Type; R : Symbolic_Expression) return Symbolic_Expression;
   function "-" (L : Scalar_Type; R : Symbolic_Expression) return Symbolic_Expression;
   function "*" (L : Scalar_Type; R : Symbolic_Expression) return Symbolic_Expression;
   function "/" (L : Scalar_Type; R : Symbolic_Expression) return Symbolic_Expression;


   package Var_Lists is
     new Ada.Containers.Indefinite_Ordered_Sets (String);

   function Free_Variables (Item : Symbolic_Expression)
                            return Var_Lists.Set;
   --  Return the name of the variables used in Item

   procedure Iterate_On_Vars (Item : Symbolic_Expression;
                              Process : access procedure (Var_Name : String));
   -- Call Process for every variable in Item


   function Replace (Item     : Symbolic_Expression;
                     Var_Name : String;
                     Value    : Scalar_Type)
                     return Symbolic_Expression;
   --  Replace every occurence of Var_Name in Item with the given scalar value


   package Variable_Tables is
     new Ada.Containers.Indefinite_Ordered_Maps
       (Key_Type     => String,
        Element_Type => Scalar_Type);
   --  Maps Variable_Tables.Maps are used to associate values to
   --  variable names

   function Replace (Item  : Symbolic_Expression;
                     Table : Variable_Tables.Map)
                     return Symbolic_Expression;
   --  For every variable in Item, check if the variable name is a key
   --  of Table and, if found, replace every occurence of the variable
   --  with the corresponding value stored in Table.

   function Replace (Item     : Symbolic_Expression;
                     Var_Name : String;
                     Value    : Symbolic_Expression)
                     return Symbolic_Expression;
   --  Replace every instance of variable Var_Name with the expression
   --  Value

   -- ============= --
   -- == PARSING == --
   -- ============= --

   type ID_Table_Type is private;
   --  The parsing function accepts, as an optional parameter, an "ID
   --  table" that specifies if an ID is a variable or a function and,
   --  in the latter case, how many parameters it accepts.
   --
   --  The behaviour of the Parse function in the presence of an ID that
   --  is not in the ID table can be one of the following
   --
   --     * Always accept
   --        (This is default) The ID is considered a variable or a
   --        function according to the presence of a '(' following the ID.
   --
   --     * Always raise an error
   --
   --     * Accept undefined variables, but not undefined functions.
   --
   --  Note that "Always accept" means "Always accept *undefined* IDs."  If,
   --  for example, the ID is registered as a function, but a '(' does not
   --  follow an error is raised.
   --

   Empty_ID_Table : constant ID_Table_Type;

   type Parameter_Count is private;
   --  A Parameter_Count stores the spec of a function in terms of number
   --  of accepted parameters.  All the combinations are possible: no
   --  parameter, an exact number of parameters, a range or any number of
   --  parameters.  To create a Parameter_Count you can use the constructors
   --  Exactly, At_Least and Between or use the constants Any_Number or
   --  No_Parameter.
   --

   function Exactly  (N : Natural) return Parameter_Count;
   function At_Least (N : Natural) return Parameter_Count;
   function Between (Min, Max : Natural) return Parameter_Count;

   Any_Number   : constant Parameter_Count;
   No_Parameter : constant Parameter_Count;

   procedure Define_Variable (Container : in out ID_Table_Type;
                              Name       : String);
   --  Declare a variable with the given name

   procedure Define_Function (Container : in out ID_Table_Type;
                              Name       : String;
                              N_Params   : Parameter_Count);
   --  Declare a function with the given name, accepting the given number
   --  of parameters.  Examples:
   --
   --     Define_Function(Table, "sin", Exactly(1));
   --     Define_Function(Table, "max", Any_Number);
   --     Define_Function(Table, "rnd", No_Parameter);
   --

   function Is_Acceptable (N_Param : Natural;
                           Limits  : Parameter_Count)
                           return Boolean;
   --  Return True if N_Param lies in Limits


   --  What to do when the parser finds an ID that is not in the
   --  ID_Table given to the parser
   type Unknown_ID_Action_Type is
     (OK,              --  Always accept the ID
      Accept_As_Var,   --  Accept it, only if used as a variable
      Die);            --  Never accept it

   Parsing_Error : exception;

   function Parse (Input         : String;
                   ID_Table      : ID_Table_Type := Empty_ID_Table;
                   On_Unknown_ID : Unknown_ID_Action_Type := OK)
                   return Symbolic_Expression;
   --  Parse a string with an expression and return the result.  The grammar
   --  of the expression is the usual one
   --
   --    Expr   = Term *(('+' | '-') Term)
   --    Term   = Fact *(('*' | '/') Fact)
   --    Fact   = [('+' | '-')] Simple
   --    Simple = Id [ '(' List ')' ] | Scalar | '(' Expr ')'
   --    List   = Expr *(',' Expr)
   --
   --  As usual, [...] denote an optional part, *(...) means
   --  one or more repetition of something and '|' means
   --  alternatives.
   --
   --  Note that
   --
   --    * In the production for Simple a single Id (without a
   --      following list) represents a variabile, while an Id with a following
   --      list represents a function call.
   --
   --      (Yes, folks, a call without arguments is something like foo(),
   --      C-like .. . I know, I know, but with this convention parsing is
   --      a bit easier since the difference is implicit in the syntax)
   --
   --    * Id follows the usual name syntax: letters, digits,
   --      underscores and (in addition) the '.', so that "foo.end" is a
   --      valid identifier.  The first char must be a letter.
   --
   --    * The syntax of Scalar is implicitely defined by the function
   --      Read_Scalar used in the instantiation.  In order to avoid the risk
   --      that the grammar above becomes ambiguous, a scalar should not begin
   --      with a letter, a '(' or a sign.



   -- =========== --
   -- == DEBUG == --
   -- =========== --

   function Dump (Item : Symbolic_Expression) return String;
   --  Return a textual representation of Item.  Useful for debugging.
   --  Currently it returns a tree-like string similar to
   --
   --     *
   --        Const  4
   --        -
   --           +
   --              Const  5
   --              Const  1
   --           Call max
   --              Var pluto.end
   --              Var pippo.end
   --
   --  The tree above is relative to the expression
   --
   --              4 * (5 + 1 - max (pluto.end, pippo.end))
   --
   --  Why does not it return a "human" string like the latter one?  Because
   --  Dump is for debugging, so it is more useful to be able to see the
   --  internal structure of Item.

private
   use Ada.Strings.Unbounded;
   use Ada.Finalization;

   --  A symbolic expression is stored as a tree, where each node has a
   --  "class" representing the operation associated with the node.

   type Node_Class is (Unary_Plus, Unary_Minus,
                       Sum, Sub, Mult, Div,
                       Fun_Call, Var, Const);

   type Node_Type (Class : Node_Class);

   type Node_Access is access Node_Type;

   type Parameter_Array is array (1..128) of Node_Access;

   type Node_Type (Class : Node_Class) is
      record
         case Class is
            when Unary_Plus | Unary_Minus =>
               Term : Node_Access;
            when Sum | Sub | Mult | Div =>
               Left, Right : Node_Access;
            when Fun_Call =>
               Fun_Name    : Unbounded_String;
               Parameters  : Parameter_Array;
               N_Params    : Natural;
            when Var =>
               Var_Name    : Unbounded_String;
            when Const =>
               Value       : Scalar_Type;
         end case;
      end record;

   function Duplicate (Item : Node_Access) return Node_Access;
   pragma Precondition (Item /= null);
   --  Create a duplicate of the tree rooted in Item

   procedure Free (Item : in out Node_Access);
   --  Release the memory associated with the tree in Item

   --
   --  Symbolic_Expression is just a thin "shell" around Node_Access.
   --  This makes recursive procedures a bit simpler and allows us
   --  to hide all the house-keeping from the user.
   --
   type Symbolic_Expression is  new
     Ada.Finalization.Controlled
   with
      record
         Expr : Node_Access;
      end record;

   overriding procedure Initialize (Item : in out Symbolic_Expression);
   overriding procedure Finalize (Item : in out Symbolic_Expression);
   overriding procedure Adjust (Item : in out Symbolic_Expression);


   type Parameter_Count is
      record
         Min : Natural;
         Max : Natural;
      end record;


   Any_Number : constant Parameter_Count := (Natural'First, Natural'Last);

   No_Parameter : constant Parameter_Count := (0, 0);

   type ID_Class is (Funct, Var);

   type ID_Descriptor (Class : ID_Class) is
      record
         case Class is
            when Funct =>
               N_Param : Parameter_Count;
            when Var =>
               null;
         end case;
      end record;

   package ID_Tables is
     new Ada.Containers.Indefinite_Ordered_Maps
       (Key_Type     => String,
        Element_Type => ID_Descriptor);

   type ID_Table_Type is
      record
         T : ID_Tables.Map := ID_Tables.Empty_Map;
      end record;


   Empty_ID_Table : constant ID_Table_Type := (T => ID_Tables.Empty_Map);

end Symbolic_Expressions;
