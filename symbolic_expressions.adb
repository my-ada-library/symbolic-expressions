----------------------------------------------------------------------------
--            Symbolic Expressions (symexpr)
--
--               Copyright (C) 2012, Riccardo Bernardini
--
--      This file is part of symexpr.
--
--      symexpr is free software: you can redistribute it and/or modify
--      it under the terms of the Lesser GNU General Public License as published by
--      the Free Software Foundation, either version 3 of the License, or
--      (at your option) any later version.
--
--      symexpr is distributed in the hope that it will be useful,
--      but WITHOUT ANY WARRANTY; without even the implied warranty of
--      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--      GNU General Public License for more details.
--
--      You should have received a copy of the Lesser GNU General Public License
--      along with gclp.  If not, see <http://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

with Ada.Unchecked_Deallocation;
with Ada.Strings.Maps;
with Ada.Strings.Fixed;
with Ada.Text_IO;

package body Symbolic_Expressions is
   --  Set to True to enable few debug prints
   Verbose : constant Boolean := False;

   ---------
   -- "+" --
   ---------

   function "+" (L : Symbolic_Expression) return Symbolic_Expression is
   begin
      return Symbolic_Expression'
         (Controlled with
             Expr => new Node_Type'(Class => Unary_Plus,
                                    Term  => Duplicate (L.Expr)));
   end "+";

   ---------
   -- "-" --
   ---------

   function "-" (L : Symbolic_Expression) return Symbolic_Expression is
   begin
      return Symbolic_Expression'
         (Controlled with
             Expr => new Node_Type'(Class => Unary_Minus,
                                    Term  => Duplicate (L.Expr)));
   end "-";

   ---------
   -- "+" --
   ---------

   function "+" (L, R : Symbolic_Expression) return Symbolic_Expression is
   begin
      return Symbolic_Expression'
         (Controlled with
             Expr => new Node_Type'(Class => Sum,
                                    Left  => Duplicate (L.Expr),
                                    Right => Duplicate (R.Expr)));
   end "+";

   ---------
   -- "-" --
   ---------

   function "-" (L, R : Symbolic_Expression) return Symbolic_Expression is
   begin
      return Symbolic_Expression'
         (Controlled with
             Expr => new Node_Type'(Class => Sub,
                                    Left  => Duplicate (L.Expr),
                                    Right => Duplicate (R.Expr)));
   end "-";

   ---------
   -- "*" --
   ---------

   function "*" (L, R : Symbolic_Expression) return Symbolic_Expression is
   begin
      return Symbolic_Expression'
         (Controlled with
             Expr => new Node_Type'(Class => Mult,
                                    Left  => Duplicate (L.Expr),
                                    Right => Duplicate (R.Expr)));
   end "*";

   ---------
   -- "/" --
   ---------

   function "/" (L, R : Symbolic_Expression) return Symbolic_Expression is
   begin
      return Symbolic_Expression'
         (Controlled with
             Expr => new Node_Type'(Class => Div,
                                    Left  => Duplicate (L.Expr),
                                    Right => Duplicate (R.Expr)));
   end "/";

   ---------
   -- "+" --
   ---------

   function "+" (L : Symbolic_Expression; R : Scalar_Type)
                 return Symbolic_Expression
   is
   begin
      return L + To_Expr (R);
   end "+";

   ---------
   -- "-" --
   ---------

   function "-" (L : Symbolic_Expression; R : Scalar_Type)
                 return Symbolic_Expression
   is
   begin
      return L - To_Expr (R);
   end "-";

   ---------
   -- "*" --
   ---------

   function "*" (L : Symbolic_Expression; R : Scalar_Type)
                 return Symbolic_Expression
   is
   begin
      return L * To_Expr (R);
   end "*";

   ---------
   -- "/" --
   ---------

   function "/" (L : Symbolic_Expression; R : Scalar_Type)
                 return Symbolic_Expression
   is
   begin
      return L / To_Expr (R);
   end "/";

   ---------
   -- "+" --
   ---------

   function "+" (L : Scalar_Type; R : Symbolic_Expression)
                 return Symbolic_Expression
   is
   begin
      return To_Expr (L) + R;
   end "+";

   ---------
   -- "-" --
   ---------

   function "-" (L : Scalar_Type; R : Symbolic_Expression)
                 return Symbolic_Expression
   is
   begin
      return To_Expr (L) - R;
   end "-";

   ---------
   -- "*" --
   ---------

   function "*" (L : Scalar_Type; R : Symbolic_Expression)
                 return Symbolic_Expression
   is
   begin
      return To_Expr (L) * R;
   end "*";

   ---------
   -- "/" --
   ---------

   function "/" (L : Scalar_Type; R : Symbolic_Expression)
                 return Symbolic_Expression
   is
   begin
      return To_Expr (L) / R;
   end "/";

   -----------------
   -- Is_Constant --
   -----------------

   function Is_Constant (X : Node_Access) return Boolean is
   begin
      case X.Class is
         when Unary_Plus | Unary_Minus =>
            return Is_Constant (X.Term);
         when Sum | Sub | Mult | Div =>
            return Is_Constant (X.Left) and Is_Constant (X.Right);
         when Fun_Call =>

            for I in 1 .. X.N_Params loop
               if not Is_Constant (X.Parameters (I)) then
                  return False;
               end if;
            end loop;

            return True;
         when Var =>
            return False;
         when Const =>
            return True;
      end case;
   end Is_Constant;

   function Is_Constant (X : Symbolic_Expression) return Boolean is
   begin
      return Is_Constant (X.Expr);
   end Is_Constant;

   -------------------
   -- Constant_Expr --
   -------------------

   function To_Expr (X : Scalar_Type) return Symbolic_Expression is
   begin
      return Symbolic_Expression'
         (Controlled with
             Expr => new Node_Type'(Class  => Const,
                                    Value  => X));
   end To_Expr;

   ---------------
   -- To_Scalar --
   ---------------

   function To_Scalar (X : Node_Access) return Scalar_Type is
   begin
      case X.Class is
         when Unary_Plus =>
            return + To_Scalar (X.Term);
         when  Unary_Minus =>
            return - To_Scalar (X.Term);
         when Sum =>
            return To_Scalar (X.Left) + To_Scalar (X.Right);

         when Sub =>
            return To_Scalar (X.Left) - To_Scalar (X.Right);

         when Mult =>
            return To_Scalar (X.Left) * To_Scalar (X.Right);

         when Div =>
            return To_Scalar (X.Left) / To_Scalar (X.Right);

         when Fun_Call =>
            declare
               Param : Scalar_Array (1 .. X.N_Params);
            begin

               for I in Param'Range loop
                  Param (I) := To_Scalar (X.Parameters (I));
               end loop;

               return Call (To_String (X.Fun_Name), Param);
            end;

         when Var =>
            raise Not_A_Scalar;

         when Const =>
            return X.Value;
      end case;
   end To_Scalar;

   function Eval (X : Symbolic_Expression) return Scalar_Type is
   begin
      return To_Scalar (X.Expr);
   end Eval;

   -------------------
   -- Function_Call --
   -------------------

   function Function_Call
      (Function_Name : String;
       Parameters    : Expression_Array)
       return Symbolic_Expression
   is
      Node : Node_Access :=
               new Node_Type'(Class      => Fun_Call,
                              Fun_Name   => To_Unbounded_String (Function_Name),
                              N_Params   => Parameters'Length,
                              Parameters => <>);
   begin
      for I in Parameters'Range loop
         Node.Parameters (I) := Duplicate (Parameters (I).Expr);
      end loop;

      return Symbolic_Expression'(Controlled with Expr => Node);
   end Function_Call;

   --------------
   -- Variable --
   --------------

   function Variable
      (Name : String)
       return Symbolic_Expression
   is
   begin
      return Symbolic_Expression'(Controlled with Expr =>
                                  new Node_Type'(Class    => Var,
                                                 Var_Name => To_Unbounded_String (Name)));
   end Variable;



   function Replace
      (Item     : Node_Access;
       Var_Name : Unbounded_String;
       Value    : Node_Access)
       return Node_Access
   is
      function Result_Class (Item     : Node_Access;
                             Var_Name : Unbounded_String) return Node_Class is
      begin
         if Item.Class = Var and then Item.Var_Name = Var_Name then
            return Const;
         else
            return Item.Class;
         end if;
      end Result_Class;
      pragma Unreferenced (Result_Class);

      Result : Node_Access;
   begin
      --        Ada.Text_IO.Put_Line ("Bibi" & Item.Class'img);

      case Item.Class is
         when Unary_Plus | Unary_Minus =>
            Result := new Node_Type (Item.Class);
            Result.Term := Replace (Item.Term, Var_Name, Value);

            pragma Assert (Result.Class = Item.Class);

         when Sum | Sub | Mult | Div =>
            Result := new Node_Type (Item.Class);
            Result.Left := Replace (Item.Left, Var_Name, Value);
            Result.Right := Replace (Item.Right, Var_Name, Value);

            pragma Assert (Result.Class = Item.Class);
         when Fun_Call =>
            Result := new Node_Type (Item.Class);
            Result.Fun_Name := Item.Fun_Name;
            Result.N_Params := Item.N_Params;

            for I in 1 .. Result.N_Params loop
               Result.Parameters (I) := Replace (Item.Parameters (I), Var_Name, Value);
            end loop;

            pragma Assert (Result.Class = Fun_Call);
         when Var =>
            --              Ada.Text_Io.Put_Line ("'" & To_String (Var_Name) & "'  '"
            --                                    & To_String (Item.Var_Name) & "'");

            if Item.Var_Name = Var_Name then
               Result := Duplicate (Value);
            else
               Result := Duplicate (Item);
            end if;

         when Const =>
            Result := Duplicate (Item);
      end case;

      --        Ada.Text_IO.Put_Line ("Bobo" & Item.Class'img);
      return Result;
   end Replace;

   -------------
   -- Replace --
   -------------

   function Replace
      (Item     : Symbolic_Expression;
       Var_Name : String;
       Value    : Scalar_Type)
       return Symbolic_Expression
   is
   begin
      return Symbolic_Expression'
         (Controlled with
             Expr => Replace (Item     => Item.Expr,
                              Var_Name => To_Unbounded_String (Var_Name),
                              Value    => To_Expr (Value).Expr));
   end Replace;

   function Replace (Item  : Symbolic_Expression;
                     Table : Variable_Tables.Map)
                     return Symbolic_Expression
   is


      Result : Symbolic_Expression := Item;

      procedure Process (Name : String) is
         use Variable_Tables;

         Pos : constant Cursor := Table.Find (Name);
      begin
         if Pos = No_Element then
            return;
         else
            Result := Replace (Item     => Result,
                               Var_Name => Name,
                               Value    => Element (Pos));
         end if;
      end Process;
   begin
      Iterate_On_Vars (Item, Process'Access);

      return Result;
   end Replace;


   function Replace (Item     : Symbolic_Expression;
                     Var_Name : String;
                     Value    : Symbolic_Expression)
                     return Symbolic_Expression
   is
   begin
      return Symbolic_Expression'
         (Controlled with
             Expr => Replace (Item.Expr, To_Unbounded_String (Var_Name), Value.Expr));
   end Replace;



   function Free_Variables (Item : Symbolic_Expression)
                            return Var_Lists.Set
   is
      procedure Fill_List (Item  : Node_Access;
                           Names : in out Var_Lists.Set)
      is
      begin
         case Item.Class is
         when Unary_Plus | Unary_Minus =>
            Fill_List (Item.Term, Names);

         when Sum | Sub | Mult | Div =>
            Fill_List (Item.Left, Names);
            Fill_List (Item.Right, Names);

         when Fun_Call =>

            for I in 1 .. Item.N_Params loop
               Fill_List (Item.Parameters (I), Names);
            end loop;

         when Var =>
            Names.Include (To_String (Item.Var_Name));

         when Const =>
            null;

         end case;
      end Fill_List;

      Result : Var_Lists.Set;
   begin
      Fill_List (Item.Expr, Result);
      return Result;
   end Free_Variables;

   ---------------------
   -- Iterate_On_Vars --
   ---------------------

   procedure Iterate_On_Vars
      (Item    : Symbolic_Expression;
       Process : access procedure (Var_Name : String))
   is
      use Var_Lists;

      --        Variables : Var_Lists.Set :=

      procedure Call (Pos : Cursor) is
      begin
         Process (Element (Pos));
      end Call;
   begin
      Free_Variables (Item).Iterate (Call'Access);
   end Iterate_On_Vars;

   ---------------
   -- Normalize --
   ---------------

   --     procedure Normalize (Item : in out Symbolic_Expression) is
   --        pragma Unreferenced (Item);
   --     begin
   --        --  Generated stub: replace with real body!
   --        pragma Compile_Time_Warning (Standard.False, "Normalize unimplemented");
   --        raise Program_Error with "Unimplemented procedure Normalize";
   --     end Normalize;

   -----------
   -- Parse --
   -----------

   function Parse (Input         : String;
                   ID_Table      : ID_Table_Type := Empty_ID_Table;
                   On_Unknown_ID : Unknown_ID_Action_Type := OK)
                   return Symbolic_Expression
   is
      Buffer       : constant String := Input;
      Cursor       : Positive;
      Current_Char : Character;
      EOS          : constant Character := Character'Val (0);

      procedure Init_Scanner is
      begin
         Cursor := Buffer'First;

         while Cursor <= Buffer'Last and then Buffer (Cursor) = ' ' loop
            Cursor := Cursor + 1;
         end loop;

         if Cursor <= Buffer'Last then
            Current_Char := Buffer (Cursor);
         else
            Current_Char := EOS;
         end if;
      end Init_Scanner;

      procedure Next_Char (Skip_Spaces : Boolean := True) is
      begin
         loop
            if Cursor >= Buffer'Last then
               Current_Char := EOS;
            else
               Cursor := Cursor + 1;
               Current_Char := Buffer (Cursor);
            end if;

            exit when (not Skip_Spaces) or Current_Char /= ' ';
         end loop;
      end Next_Char;

      procedure Expect (What : Character) is
      begin
         if Current_Char /= What then
            raise Parsing_Error
               with "Expecting '" & What & "' got '" & Current_Char & "'";
         else
            Next_Char;
         end if;
      end Expect;

      function Remaining return String is
      begin
         return Buffer (Cursor .. Buffer'Last);
      end Remaining;

      Level : Natural := 0;

      function Indent return String is
         use Ada.Strings.Fixed;
      begin
         return (Level * 3) * " ";
      end Indent;

      procedure Down_Level is
      begin
         Level := Level + 1;
      end Down_Level;

      procedure Up_Level is
      begin
         Level := Level - 1;
      end Up_Level;

      procedure Ecco (X : String) is
      begin
         if Verbose then
            Ada.Text_Io.Put_Line (Indent
                                  & "Calling "
                                  & X & "[" & Remaining & "]"
                                  & "'" & Current_Char & "'");
            Down_Level;
         end if;
      end Ecco;

      procedure Fine is
      begin
         if Verbose then
            Up_Level;
            Ada.Text_Io.Put_Line (Indent & "done " & "[" & Remaining & "]"
                                  & "'" & Current_Char & "'");
         end if;
      end Fine;

      procedure Advance (N : Positive) is
      begin
         Cursor := Cursor + N - 1;
         Next_Char;
      end Advance;

      function Parse_Expr return Node_Access;

      function Parse_Identifier return Unbounded_String is
         use Ada.Strings.Maps;

         Result   : Unbounded_String;
         ID_Chars : constant Character_Set :=
                      To_Set (Character_Range'('a', 'z')) or
            To_Set (Character_Range'('A', 'Z')) or
            To_Set (Character_Range'('0', '9')) or
            To_Set ('.') or
            To_Set ('_');
      begin
         Ecco ("ID");

         while Is_In (Current_Char, ID_Chars) loop
            Result := Result & Current_Char;
            Next_Char (Skip_Spaces => False);
         end loop;

         if Current_Char = ' ' then
            Next_Char;
         end if;

         Fine;
         return Result;
      end Parse_Identifier;

      procedure Parse_Parameter_List (Parameters : out Parameter_Array;
                                      N_Params   : out Natural)
      is
      begin
         Ecco ("par-list");

         if Current_Char = ')' then
            N_Params := 0;

         else
            N_Params := 1;
            Parameters (1) := Parse_Expr;

            while Current_Char = ',' loop
               Next_Char;
               N_Params := N_Params + 1;
               Parameters (N_Params) := Parse_Expr;
            end loop;
         end if;

         Expect (')');
         Fine;
      exception
         when others =>
            for I in Parameters'First .. N_Params - 1 loop
               Free (Parameters (I));
            end loop;

            raise;
      end Parse_Parameter_List;


      function Parse_Simple return Node_Access is
         use ID_Tables;

         Result     : Node_Access := null;
         Parameters : Parameter_Array;
         N_Params   : Natural;
         Identifier : Unbounded_String;
         Pos        : ID_Tables.Cursor;
      begin
         Ecco ("simple");

         case Current_Char is
            when 'a' .. 'z' | 'A' .. 'Z' =>
               Identifier := Parse_Identifier;

               Pos := ID_Table.T.Find (To_String (Identifier));

               if Pos = No_Element and On_Unknown_ID = Die then
                  raise Parsing_Error
                     with "Unknown ID '" & To_String (Identifier) & "'";
               end if;

               if Current_Char = '(' then

                  if Pos = No_Element then
                     if On_Unknown_ID /= OK then
                        raise Parsing_Error
                           with "Unknown ID '" & To_String (Identifier) & "'";
                     end if;
                  else
                     if Element (Pos).Class /= Funct then
                        raise Parsing_Error
                           with "Variable '" & To_String (Identifier) & "' used as function";
                     end if;
                  end if;

                  Next_Char;

                  Parse_Parameter_List (Parameters, N_Params);

                  if Pos /= No_Element then
                     pragma Assert (Element (Pos).Class = Funct);

                     if not Is_Acceptable (N_Params, Element (Pos).N_Param) then
                        raise Parsing_Error
                           with "Wrong # of arguments";
                     end if;
                  end if;


                  Result := new Node_Type'(Class      => Fun_Call,
                                           Fun_Name   => Identifier,
                                           Parameters => Parameters,
                                           N_Params   => N_Params);
               else
                  if Pos /= No_Element and then Element (Pos).Class /= Var then
                     raise Parsing_Error
                        with "Function '" & To_String (Identifier) & "' used as variable";
                  end if;

                  Result := new Node_Type'(Class    => Var,
                                           Var_Name => Identifier);
               end if;
            when '(' =>
               Next_Char;
               Result := Parse_Expr;
               Expect (')');

            when others =>
               declare
                  Success  : Boolean;
                  Val      : Scalar_Type;
                  Consumed : Natural;
               begin
                  Read_Scalar (Remaining, Success, Consumed, Val);

                  if not Success then
                     raise Parsing_Error;
                  else
                     Advance (Consumed);

                     Result := new Node_Type'(Class => Const,
                                              Value => Val);
                  end if;
               end;
         end case;

         Fine;
         return Result;
      exception
         when others =>
            Free (Result);
            raise;
      end Parse_Simple;

      function Parse_Fact return Node_Access is
         Result   : Node_Access := null;
         Operator : Character;
         Operand  : Node_Access;
      begin
         Ecco ("fact");

         if Current_Char = '+' or Current_Char = '-' then
            Operator := Current_Char;
            Next_Char;

            Operand := Parse_Simple;

            case Operator is
               when '+' =>
                  Result := new Node_Type'(Class => Unary_Plus,
                                           Term  => Operand);

               when '-' =>
                  Result := new Node_Type'(Class => Unary_Minus,
                                           Term  => Operand);

               when others =>
                  -- We should never arrive here
                  raise Program_Error;
            end case;
         else
            Result := Parse_Simple;
         end if;

         Fine;
         return Result;
      exception
         when others =>
            Free (Result);
            raise;
      end Parse_Fact;

      function Parse_Term return Node_Access is
         Result   : Node_Access := null;
         Operator : Character;
         Operand  : Node_Access;
      begin
         Ecco ("term");

         Result := Parse_Fact;

         while Current_Char = '*' or Current_Char = '/' loop
            Operator := Current_Char;
            Next_Char;

            Operand := Parse_Fact;
            case Operator is
               when '*' =>
                  Result := new Node_Type'(Class => Mult,
                                           Left  => Result,
                                           Right => Operand);

               when '/' =>
                  Result := new Node_Type'(Class => Div,
                                           Left  => Result,
                                           Right => Operand);

               when others =>
                  -- We should never arrive here
                  raise Program_Error;
            end case;
         end loop;

         Fine;
         return Result;
      exception
         when others =>
            Free (Result);
            raise;
      end Parse_Term;

      function Parse_Expr return Node_Access is
         Result   : Node_Access := null;
         Operator : Character;
         Operand  : Node_Access;
      begin
         Ecco ("expr");

         Result := Parse_Term;

         while Current_Char = '+' or Current_Char = '-' loop
            Operator := Current_Char;
            Next_Char;

            Operand := Parse_Term;
            case Operator is
               when '+' =>
                  Result := new Node_Type'(Class => Sum,
                                           Left  => Result,
                                           Right => Operand);

               when '-' =>
                  Result := new Node_Type'(Class => Sub,
                                           Left  => Result,
                                           Right => Operand);

               when others =>
                  -- We should never arrive here
                  raise Program_Error;
            end case;
         end loop;

         Fine;
         return Result;
      exception
         when others =>
            Free (Result);
            raise;
      end Parse_Expr;

      Result : Symbolic_Expression;
   begin
      Init_Scanner;

      Result.Expr := Parse_Expr;

      return Result;
   end Parse;

   function Duplicate (Item : Node_Access) return Node_Access
   is
      Result : Node_Access := new Node_Type (Item.Class);
   begin
      if Item = null then
         return null;
      end if;

      case Item.Class is
         when Unary_Plus | Unary_Minus =>
            Result.Term := Duplicate (Item.Term);
         when Sum | Sub | Mult | Div =>
            Result.Left := Duplicate (Item.Left);
            Result.Right := Duplicate (Item.Right);
         when Fun_Call =>
            Result.Fun_Name := Item.Fun_Name;
            Result.N_Params := Item.N_Params;

            for I in 1 .. Result.N_Params loop
               Result.Parameters (I) := Duplicate (Item.Parameters (I));
            end loop;
         when Var =>
            Result.Var_Name := Item.Var_Name;
         when Const =>
            Result.Value := Item.Value;
      end case;

      return Result;
   end Duplicate;

   procedure Free (Item : in out Node_Access)
   is
      procedure Dealloc is
         new Ada.Unchecked_Deallocation (Object => Node_Type,
                                         Name   => Node_Access);
   begin
      if Item = null then
         return;
      end if;

      pragma Assert (Item /= null);

      case Item.Class is
         when Unary_Plus | Unary_Minus =>
            Free (Item.Term);
         when Sum | Sub | Mult | Div =>
            Free (Item.Left);
            Free (Item.Right);
         when Fun_Call =>
            for I in 1 .. Item.N_Params loop
               Free (Item.Parameters (I));
            end loop;
         when Var =>
            null;
         when Const =>
            null;
      end case;

      Dealloc (Item);
   end Free;

   function Dump (Item      : Node_Access;
                  Level     : Natural)
                  return String
   is
      use Ada.Strings.Fixed;

      function Head (X : String) return Unbounded_String is
      begin
         return To_Unbounded_String (((Level * 3) * " ") & X);
      end Head;

      CRLF   : constant String := Character'Val (13) & Character'Val (10);
      Result : Unbounded_String;
   begin
      case Item.Class is
         when Unary_Plus =>
            Result := Head ("@+") & CRLF & Dump (Item.Term, Level + 1);

         when  Unary_Minus =>
            Result := Head ("@-") & CRLF & Dump (Item.Term, Level + 1);

         when Sum =>
            Result := Head ("+")
               & CRLF & Dump (Item.Left, Level + 1)
               & CRLF & Dump (Item.Right, Level + 1);

         when Sub =>
            Result := Head ("-")
               & CRLF & Dump (Item.Left, Level + 1)
               & CRLF & Dump (Item.Right, Level + 1);

         when Mult =>
            Result := Head ("*")
               & CRLF & Dump (Item.Left, Level + 1)
               & CRLF & Dump (Item.Right, Level + 1);

         when Div =>
            Result := Head ("/")
               & CRLF & Dump (Item.Left, Level + 1)
               & CRLF & Dump (Item.Right, Level + 1);

         when Fun_Call =>
            Result := Head ("Call " & To_String (Item.Fun_Name));

            for I in 1 .. Item.N_Params loop
               Result := Result & CRLF & Dump (Item.Parameters (I), Level + 1);
            end loop;

         when Var =>
            Result := Head ("Var ") & To_String (Item.Var_Name);

         when Const =>
            Result := Head ("Const ") & Image (Item.Value);
      end case;

      return To_String (Result);
   end Dump;


   function Dump (Item : Symbolic_Expression) return String
   is
   begin
      return Dump (Item.Expr, 0);
   end Dump;

   overriding procedure Initialize (Item : in out Symbolic_Expression)
   is
   begin
      Item.Expr := null;
   end Initialize;

   overriding procedure Finalize (Item : in out Symbolic_Expression)
   is
   begin
      if Item.Expr /= null then
         Free (Item.Expr);
      end if;
   end Finalize;

   overriding procedure Adjust (Item : in out Symbolic_Expression)
   is
   begin
      if Item.Expr /= null then
         Item.Expr := Duplicate (Item.Expr);
      end if;
   end Adjust;

   function Exactly  (N : Natural) return Parameter_Count
   is
   begin
      return Parameter_Count'(Min => N, Max => N);
   end Exactly;

   function At_Least (N : Natural) return Parameter_Count
   is
   begin
      return Parameter_Count'(Min => N, Max => Natural'Last);
   end At_Least;

   function Between (Min, Max : Natural) return Parameter_Count
   is
   begin
      return Parameter_Count'(Min => Min, Max => Max);
   end Between;

   function Is_Acceptable (N_Param : Natural;
                           Limits  : Parameter_Count)
                           return Boolean
   is
   begin
      return N_Param >= Limits.Min and N_Param <= Limits.Max;
   end Is_Acceptable;

   ---------------------
   -- Define_Variable --
   ---------------------

   procedure Define_Variable (Container  : in out ID_Table_Type;
                              Name       : String)
   is
   begin
      Container.T.Include (Name, (Class => Var));
   end Define_Variable;

   ---------------------
   -- Define_Function --
   ---------------------

   procedure Define_Function (Container  : in out ID_Table_Type;
                              Name       : String;
                              N_Params   : Parameter_Count)
   is
   begin
      Container.T.Include (Name, (Class => Funct, N_Param => N_Params));
   end Define_Function;

end Symbolic_Expressions;
